#include <stdio.h>
#include <stdlib.h>
#include "exercice.h"

void print_array(int array[], int length);

int main(int argc, char *argv[])
{
	int array1[10] = {13, 82, 23, 94, 505, 6, 1337, 98, 69, -42};
	int array2[10] = {0};
	int sum;
	double ave;
	
	print_array(array1, 10);
	
	sum = sum_array(array1, 10);
	printf("Sum of the array : %d\n", sum);
	
	ave = average_array(array1, 10);
	printf("The average of the array is : %f\n", ave);
	
	copy_array(array1, array2, 10);
	print_array(array2, 10);
	
	major_array(array1, 10, 42);
	print_array(array1, 10);
	
	sort_array(array2, 10);
	print_array(array2, 10);

	return 0;
}

void print_array(int array[], int length)
{
	int i;

	printf("[");
	for (i = 0; i < length - 1; i++)
		printf("%d; ", array[i]);
	printf("%d]\n", array[length - 1]);
}
