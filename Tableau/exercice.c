#include <stdio.h>
#include <stdlib.h>
#include "exercice.h"

// Exercice 1
int sum_array(int array[], int length)
{
	int i, sum = 0;

	for (i = 0; i < length; i++)
		sum += array[i];

	return sum;
}

// Exercice 2
double average_array(int array[], int length)
{
	int i;
	double ave = 0.;

	for (i = 0; i < length; i++)
		ave += array[i];

	return (ave / length);
}

// Exercice 3
void copy_array(int array1[], int array2[], int length)
{
	int i;

	for (i = 0; i < length; i++)
		array2[i] = array1[i];
}

// Exercice 4
void major_array(int array[], int length, int max)
{
	int i;

	for (i = 0; i < length; i++)
		if (array[i] > max)
			array[i] = 0;
}

// Exercice 5
void sort_array(int array[], int length)
{
	int i, j, aux, pos = 0;

	for (i = 0; i < length; i++)
	{
		pos = i;

		for (j = i + 1; j < length; j++)
			if (array[j] < array[pos])
				pos = j;

		// Optimisation : Stop those step when the sort is finish
		aux = array[i];
		array[i] = array[pos];
		array[pos] = aux;
	}
}
