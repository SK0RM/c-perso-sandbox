#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
	srand(time(NULL));

	// Variables
	int max, min, answer, randy, count;

	// Initilisation
	max = 100;
	min = 0;
	randy = rand() % (max - min) + min;
	count = 0;

	// Loop
	do
	{
		printf("Quel est le nombre mystere ?\n");
		scanf("%d", &answer);

		count++;

		if (answer == randy)
			printf("Bravo ! Vous avez trouvez en %d coups.\n", count);
		else if (answer < randy)
			printf("C'est plus !\n\n");
		else
			printf("C'est moins !\n\n");

	}while(answer != randy);

	return 0;
}

